def sumar(arreglo):
	if len(arreglo)==0:
		return 0
	else:
		return arreglo.pop()+sumar(arreglo)

print("Main> sumar [5,4,7,8] \n"+str(sumar ([5,4,7,8])))

def invertir(arreglo):
	arreglo.reverse()
	return arreglo

print("Main> invertir [5,4,7,8] \n"+str(invertir([5,4,7,8])))


def igualLista(lista1, lista2):
	if lista1 == lista2:
		return True
	else:
		return False

print("Main> igualLista [\"Hola\",\"Mundo\"] [\"Mundo\",\"Hola\"]\n"+ str(igualLista(["Hola","Mundo"], ["Mundo","Hola"])))

def listaOrdenada(lista):
	if len(lista)==0:
		return True
	else :
		return lista.pop()>lista[len(lista)-1] and listaOrdenada(lista)
			
print("Main> lista_ordenada [3, 2, 5, 7] \n"+str(listaOrdenada([3, 2, 5, 7])))

def mostrarUbicacion(lista, index):
	return lista[index]

print("Main> mostrar_ubicacion [15,25,26,28] 2 \n"+str(mostrarUbicacion([15,25,26,28], 2)))

def mayor(lista):
	if len(lista)==1:
		return lista[0]
	else:
		a = lista.pop()
		if(a>mayor(lista)):
			return a
		else:
			return mayor(lista)

print("Main> mayor [78,24,56,93,21,237,46,74,125] \n"+str(mayor([78,24,56,93,21,237,46,74,125])))

def contarPares(lista):
	return len([x for x in lista if x%2==0])

print("Main> contarPares [5,4,7,8] \n"+str(contarPares([5,4,8,8])))

def cuadrados(lista):
	return [x**2 for x in lista]

print("Main> cuadrados [1..10] \n"+str(cuadrados([x for x in range(1, 10)])))

def divisible(num1, num2):
	return num1 % num2 == 0

def divisibles(num):
	return [y for y in range(1, num) if divisible(num, y)]

def esPrimo(num):
	return len(divisibles(num)) < 2

def primos(num):
	return [x for x in range(1,num) if esPrimo(x)]

print("Main> primos 100 \n"+str(primos(100)))